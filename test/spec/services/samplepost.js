'use strict';

describe('Service: SamplePost', function () {

  // load the service's module
  beforeEach(module('httpPostApp'));

  // instantiate service
  var SamplePost;
  beforeEach(inject(function (_SamplePost_) {
    SamplePost = _SamplePost_;
  }));

  it('should do something', function () {
    expect(!!SamplePost).toBe(true);
  });

});
