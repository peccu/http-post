'use strict';

/**
 * @ngdoc function
 * @name httpPostApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the httpPostApp
 */
angular.module('httpPostApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
