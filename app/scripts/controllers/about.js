'use strict';

/**
 * @ngdoc function
 * @name httpPostApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the httpPostApp
 */
angular.module('httpPostApp')
  .controller('AboutCtrl', ['$scope', 'SamplePost', function ($scope, SamplePost) {
    // var util = require('util');

    $scope.account = {name: 'hoge', password: 'fuga'};
    $scope.save = function(){
      var data = {
        name: $scope.account.name,
        password: $scope.account.password
      };
      SamplePost.save(data).success(function(data, status, headers, config){
        var response = {data: data, status: status, headers: headers, config: config};
        console.log('success');
        // console.log(util.inspect(response, {colors: true, depth: null}));
        console.log(angular.toJson(response,true));
      }).error(function(data, status, headers, config){
        var response = {data: data, status: status, headers: headers, config: config};
        console.log('error');
        // console.log(util.inspect(response, {colors: true, depth: null}));
        console.log(angular.toJson(response,true));
      });
    };
  }]);
