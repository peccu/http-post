'use strict';

/**
 * @ngdoc service
 * @name httpPostApp.SamplePost
 * @description
 * # SamplePost
 * Factory in the httpPostApp.
 */
angular.module('httpPostApp')
  .factory('SamplePost', ['$http', function($http){
    var uri = 'http://localhost:3000/post';
    var samplePost = {};
    samplePost.save = function(data){
      return $http.post(uri, data);
    };

    return samplePost;

//     // http://weblogs.asp.net/dwahlin/using-an-angularjs-factory-to-interact-with-a-restful-service
//     var urlBase = '/api/customers';
//     var dataFactory = {};
//     dataFactory.getCustomers = function () {
//       return $http.get(urlBase);
//     };
//     dataFactory.getCustomer = function (id) {
//       return $http.get(urlBase + '/' + id);
//     };
//     dataFactory.insertCustomer = function (cust) {
//       return $http.post(urlBase, cust);
//     };
//     dataFactory.updateCustomer = function (cust) {
//       return $http.put(urlBase + '/' + cust.ID, cust);
//     };
//     dataFactory.deleteCustomer = function (id) {
//       return $http.delete(urlBase + '/' + id);
//     };
//     dataFactory.getOrders = function (id) {
//       return $http.get(urlBase + '/' + id + '/orders');
//     };
//     return dataFactory;

  }]);
