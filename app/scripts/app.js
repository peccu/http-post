'use strict';

/**
 * @ngdoc overview
 * @name httpPostApp
 * @description
 * # httpPostApp
 *
 * Main module of the application.
 */
angular
  .module('httpPostApp', [
    'ngRoute'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
